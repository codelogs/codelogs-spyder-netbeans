## SPYDER PLUGIN NETBEANS

Este plugin es un enlace entre el servicio spyder y Netbeans, en esta versión
el plugin contiene una distribución de spyder, para futuras versiones, spyder sera 
descargado automaticamente de los repositorios oficiales, durante el proceso
de instalacion del plugin.

### Descargar desde contruicciones diarias.

Recuerda usar la ultima línea de construcción, ya que estas representa las versiones
mas recientes. 

https://gitlab.com/codelogs/codelogs-spyder-netbeans/builds

### Limitaciones
En caso de no existir ningun servicio en ejecución, Netbeans ejecutara
un proceso con las siguientes limitaciones :
 - Para reducir consumo de memoria se utilizan las API de eventos propias del IDE, en lugar
 de las implementadas en spyder
 - Cuando se cierra el IDE el servicio se detiene.
 - la ubicación/home de spyder por defecto sera en la carpeta home, de windows / linux / mac
 
 
  