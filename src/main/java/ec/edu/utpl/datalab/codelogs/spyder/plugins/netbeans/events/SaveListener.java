/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.events;

import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options.Spyder;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.spi.editor.document.OnSaveTask;
import org.openide.filesystems.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.text.Document;


public class SaveListener implements OnSaveTask {
    private static final Logger log = LoggerFactory.getLogger(SaveListener.class);
    private final Document document;

    private SaveListener(Document document) {
        this.document = document;
    }

    @Override
    public void performTask() {
        final FileObject file = this.getFile();
        if (file != null) {
            final Project currentProject = this.getProject();
            //CompilationBehavior.configureCompileLogs(currentProject);
            log.info("Event nbm fire");
            Spyder.writeCmdOpen(currentProject);
            Spyder.writeHeartBeat(file.getPath(), MonitorCommand.EVENT_VALUE_UPDATE);
        }
    }

    @Override
    public void runLocked(Runnable r) {
        r.run();
    }

    @Override
    public boolean cancel() {
        return true;
    }

    private FileObject getFile() {
        if (this.document == null)
            return null;
        Source source = Source.create(this.document);
        if (source == null)
            return null;
        FileObject fileObject = source.getFileObject();
        if (fileObject == null)
            return null;
        return fileObject;
    }

    private Project getProject() {
        if (this.document == null)
            return null;
        Source source = Source.create(this.document);
        if (source == null)
            return null;
        FileObject fileObject = source.getFileObject();
        if (fileObject == null)
            return null;
        return FileOwnerQuery.getOwner(fileObject);
    }

    @MimeRegistration(mimeType = "", service = OnSaveTask.Factory.class, position = 1500)
    public static final class FactoryImpl implements Factory {

        @Override
        public OnSaveTask createTask(Context context) {
            log.info("Save capture event");
            return new SaveListener(context.getDocument());
        }
    }

}