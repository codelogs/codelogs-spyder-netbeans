/*
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.SocketPack;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.Installer;
import org.openide.awt.NotificationDisplayer;
import org.openide.awt.StatusLineElementProvider;
import org.openide.util.ImageUtilities;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.ServiceProvider;
import rx.Observer;

import javax.inject.Singleton;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 *
 * @author rfcardenas
 */
@Singleton
@ServiceProvider(service = StatusLineElementProvider.class)
public class StatusIconColdeogs implements StatusLineElementProvider, Observer<SocketPack> {

    private static JLabel info;
    private static Observer<SocketPack> observer;
    private static RequestProcessor requestProcessor = new RequestProcessor(Installer.class);

    static {
        Image iconpreset = ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/sync-alert.png");
        Icon icon = new ImageIcon(iconpreset);
        info = new JLabel(icon);
        observer = new StatusIconColdeogs();
    }

    @Override
    public Component getStatusLineElement() {
        return info;
    }

    public static void updateIconSync() {
        Image iconpreset = ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/sync.png");
        Icon icon = new ImageIcon(iconpreset);
        info.setIcon(icon);
        info.setText("Sync");

    }

    public static void updateIconSend() {
        Image iconpreset = ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/cube-send.png");
        Icon icon = new ImageIcon(iconpreset);
        info.setIcon(icon);
        info.setText("Sync");
    }

    public static void updateErrorIcon() {
        Image iconpreset = ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/sync-alert.png");
        Icon icon = new ImageIcon(iconpreset);
        info.setIcon(icon);
        info.setText("Error");

    }

    public static void updateCloudIcon() {
        Image iconpreset = ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/cloud-sync.png");
        Icon icon = new ImageIcon(iconpreset);
        info.setIcon(icon);
        info.setText("Cloud");
        NotificationDisplayer.getDefault().notify("Codelogs conectado", icon, "proceso conectado", (ActionEvent e) -> {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        });
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable thrwbl) {
        requestProcessor.execute(() -> {
            updateCloudIcon();
        });
    }

    @Override
    public void onNext(SocketPack t) {
        if (t.getPack() == SocketPack.PACK.CONNECT_SUCCESS) {
            requestProcessor.execute(() -> {
                updateCloudIcon();
            });
        }
        if (t.getPack() == SocketPack.PACK.CONNECT_ERROR) {
            requestProcessor.execute(() -> {
                updateErrorIcon();
            });
        }
    }

}
