/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top;

import java.beans.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import org.openide.explorer.view.OutlineView;


public class FixedColumnTable implements ChangeListener, PropertyChangeListener {

    private JTable main;
    private OutlineView fixed;
    private JScrollPane scrollPane;

    
    public FixedColumnTable(int fixedColumns, JScrollPane scrollPane) {
        this.scrollPane = scrollPane;

        main = ((JTable) ((OutlineView)scrollPane.getViewport().getView()).getOutline());
        main.setAutoCreateColumnsFromModel(false);
        main.addPropertyChangeListener(this);




        int totalColumns = main.getColumnCount();

        fixed = new OutlineView("Tree");
        fixed.getOutline().setAutoCreateColumnsFromModel(false);
        fixed.getOutline().setModel(main.getModel());
        fixed.getOutline().setSelectionModel(main.getSelectionModel());
        fixed.getOutline().setFocusable(false);





            TableColumnModel columnModel = main.getColumnModel();
            TableColumn column = columnModel.getColumn(0);
            columnModel.removeColumn(column);





        fixed.getOutline().setPreferredScrollableViewportSize(fixed.getPreferredSize());
        scrollPane.setRowHeaderView(fixed);
        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, fixed.getOutline().getTableHeader());



        scrollPane.getRowHeader().addChangeListener(this);
    }

    
    public JTable getFixedTable() {
        return fixed.getOutline();
    }




    public void stateChanged(ChangeEvent e) {


        JViewport viewport = (JViewport) e.getSource();
        scrollPane.getVerticalScrollBar().setValue(viewport.getViewPosition().y);
    }




    public void propertyChange(PropertyChangeEvent e) {


        if ("selectionModel".equals(e.getPropertyName())) {
            fixed.getOutline().setSelectionModel(main.getSelectionModel());
        }

        if ("model".equals(e.getPropertyName())) {
            fixed.getOutline().setModel(main.getModel());
        }
    }
}
