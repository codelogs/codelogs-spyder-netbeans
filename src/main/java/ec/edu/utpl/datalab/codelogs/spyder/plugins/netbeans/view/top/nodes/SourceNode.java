/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.PublicResult;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeAbstractNode;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFileCode;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.MetricsWindowTopComponent;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes.factories.FileChildFactory;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class SourceNode extends AbstractNode {

    PublicResult metricResult;

    public SourceNode(NodeFileCode file, PublicResult metricResult) {
        super(Children.create(new FileChildFactory(file, metricResult), true));
        this.file = file;
        this.metricResult = metricResult;
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        if (file != null) {

            List<MetricVerticle> collection = metricResult.loadAllMetrics(NodeKink.SOURCE);
            System.out.println(" SIZE  METRIC : " + collection.size());
            System.out.println(" AbstractNodes " + file.getNodeAbstractTrees().size());
            TreeTypeSpliterator<NodeAbstractNode> spliterator = TreeTypeSpliterator.create(file, NodeAbstractNode.class);
            List<NodeAbstractNode> abstractNodes = StreamSupport.stream(spliterator, false)
                    .collect(Collectors.toList());
            
            for (NodeAbstractNode abstractNode : abstractNodes) {
                for (MetricVerticle metric : collection) {
                    Measure result = metricResult.loadSingleByName(abstractNode.getName(), metric);
                    System.out.println("Metric " + metric.getSpecification().getName());
                    System.out.println("#Node  " + file.getElementID());
                    System.out.println("--> " + result);
                    if (result != null) {
                        double value = result.getResult();
                        System.out.println("Measure File>>>" + value);
                        String name = metric.getSpecification().getName();
                        String desc = metric.getSpecification().getHelp();
                        set.put(new MetricPropertyDouble(name, value, desc));
                    }
                }
                break;
            }
        }
        sheet.put(set);
        return sheet;
    }

    @Override
    public String getHtmlDisplayName() {
        if (file == null) {
            MetricsWindowTopComponent.getSelf().setMessage(false, "No files have been measured yet.");
            return "none";
        } else {
            return file.getName();
        }
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getIcon(type);
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/class.png");
    }
    private NodeFileCode file;
}
