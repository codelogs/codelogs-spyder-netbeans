/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.SocketPack;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.WritterJson;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.CodeAnalysisPack;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.utils.OutWritterRx;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.StatusIconColdeogs;
import ec.edu.utpl.datalab.codelogs.spyder.shell.monitor.*;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.netbeans.api.project.Project;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;
import rx.Subscription;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem.newSearch;
import static ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.utils.SyderUtils.*;

public class Spyder {
    private static final Logger log = LoggerFactory.getLogger(Spyder.class);

    public static final String IDE = "NETBEANS";
    private static Set<String> setClose;
    private static Thread thread;
    private static AutoConnectionSocket systemSocket;


    /**
     * Conexion 
     * */
    static {
        int port = NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT, 8484);
        setClose = new HashSet<>();
        systemSocket = new AutoConnectionSocket(port, 10, 1, TimeUnit.SECONDS);
        Subscription subscribe = systemSocket.getChannel().filter(pack -> pack.getPack() != SocketPack.PACK.MESSAGE)
                .subscribe(new StatusIconColdeogs());

        systemSocket.getChannel()
                .subscribe((SocketPack t) -> {
                    if (t.getPack() == SocketPack.PACK.CONNECT_ERROR) {
                        error(t.getData());
                    }
                    if (t.getPack() == SocketPack.PACK.MESSAGE) {
                        info(t.getData());
                    }
                });


    }

    /**
     * Write command open project
     *
     * @param project
     */
    public static void writeCmdOpen(Project project) {
        System.out.println("Enviando comando " + systemSocket.isConnected());
        int port = NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT, 8484);
        try {
            ShellOpenProject openProject = new ShellOpenProject(systemSocket);
            String path = project.getProjectDirectory().getPath();
            path = formatUniversal(path);
            openProject.open(path, Spyder.IDE);
            registerToClose(project);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeHeartBeat(String path, String type) {
        ShellEventFile eventFile = new ShellEventFile(systemSocket);
        try {
            eventFile.eventFile(path, type);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeCmdClose() {
        int port = NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT, 8484);
        for (String project : setClose) {
            try {
                Spyder.info("Close project " + project);
                ShellCloseProject openProject = new ShellCloseProject(systemSocket);
                openProject.close(project);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    public static void writeCmdKill() {
        try {
            info("Kill monitor");
            ShellShutDownMonitor ckmw = new ShellShutDownMonitor(systemSocket);
            ckmw.kill();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeCmdMetricsCollect(CodeAnalysisPack analysis) {
        System.out.println("Enviando comando " + systemSocket.isConnected());
        try {
            ShellCollectMetrics collectMetrics = new ShellCollectMetrics(systemSocket);
            collectMetrics.sendMetrics(WritterJson.writte(analysis));
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeCommand(String command) {
        try {
            System.out.println("Enviando comandos " + command + " status " + systemSocket.isConnected());
            systemSocket.send(command);
        } catch (Exception e) {
            log.error("Imposible enviar comando");
        }
    }

    private static void registerToClose(Project p) {
        String project = p.getProjectDirectory().getPath();
        setClose.add(project);
    }

    public static boolean isConfig() {
        String keyap = Global.SYNC_APIKEY;
        String keyServer = Global.SYNC_SERVER;
        String keyProtocol = Global.SYNC_PROTOCOL;

        String server = NbPreferences.forModule(AjustesPanel.class).get(keyServer, "");
        String protoco = NbPreferences.forModule(AjustesPanel.class).get(keyProtocol, "");
        String api = NbPreferences.forModule(AjustesPanel.class).get(keyap, "");

        if (api.isEmpty() || server.isEmpty() || protoco.isEmpty()) {
            System.out.println("Sout no se inicio el server [sin configuraciones]");
            return false;
        }
        return true;
    }

    private static List<String> makeCmd(FileObject jar) {
        String apikey = NbPreferences.forModule(AjustesPanel.class).get(Global.SYNC_APIKEY, "");
        String protoco = NbPreferences.forModule(AjustesPanel.class).get(Global.SYNC_PROTOCOL, "");
        String server = NbPreferences.forModule(AjustesPanel.class).get(Global.SYNC_SERVER, "");
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG, false);
        boolean fsmEnable = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_ENABLE_EVENTSYSTEM_INHERITED, true);

        ArrayList<String> command = new ArrayList<String>();
        command.add("java");
        command.add("-Xmx38m");
        command.add("-XX:MaxMetaspaceSize=38m");
        command.add("-jar");
        command.add((jar.getName().getPath()));
        command.add(String.format("%s=%s", Global.MONITOR_DEBUG, debug));
        command.add(String.format("%s=%s", Global.MONITOR_ENABLE_EVENTSYSTEM_INHERITED, !fsmEnable));
        command.add(String.format("%s=%s", Global.SYNC_APIKEY, apikey));
        command.add(String.format("%s=%s", Global.SYNC_SERVER, server));
        command.add(String.format("%s=%s", Global.SYNC_PROTOCOL, protoco));
        return command;
    }

    /**
     * public static void attemptConnectProcess() { try {
     * System.out.println("Intendo conectar al proceso"); int valuePort =
     * NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT,
     * 8484); if (!systemSocket.isConnected()) {
     * systemSocket.forceConnection(valuePort); System.out.println("Resultado "
     * + systemSocket.isConnected()); }
     * <p>
     * } catch (InterruptedException ex) { Exceptions.printStackTrace(ex); } }*
     */
    public static void start() {
        try {
            SearchFileSystem search = newSearch(3)
                    .withFilter(FilterSet.filterFileNameContain(SPYDER_SERVICE_JAR))
                    .build();
            FileObject home = VFS.getManager().resolveFile(HOME_SPYDER + File.separatorChar + FOLDER_NAME_SPYDER);

            FileObject[] results = search.executeFinder(home);
            if(results.length==0) {
                error("imposible encontrar jar service en home " + home);
                return;
            }
            FileObject jar = results[0];
            info("Iniciando monitor " + systemSocket.isConnected());
            info("Server monitor is offline " + isOfflineProcess());
            if (isOfflineProcess()) {
                if (Spyder.isConfig()) {
                    info("Thread lanzado");
                    Runnable procRunnable = () -> {
                        try {
                            List<String> comando = makeCmd(jar);

                            //  String command = "java -jar " + proc + " " + Spyder.loadPreferencesArg();
                            InputOutput out = IOProvider.getDefault().getIO("spyder", false);

                            OutWritterRx errorsink = new OutWritterRx(out.getErr());
                            OutWritterRx outsink = new OutWritterRx(out.getOut());

                            Spyder.info("Comando " + comando);

                            new ProcessExecutor().command(comando)
                                    .environment(Global.MONITOR_DEBUG, "true")
                                    .readOutput(true)
                                    .redirectError(errorsink)
                                    .redirectOutput(outsink)
                                    .execute()
                                    .outputUTF8();

                        } catch (IOException | InterruptedException | TimeoutException | InvalidExitValueException ex) {
                            Exceptions.printStackTrace(ex);
                        }
                    };

                    thread = new Thread(procRunnable);
                    thread.setName("Monitor Thread");
                    thread.setUncaughtExceptionHandler((Thread t, Throwable e) -> {
                        log.error("Error Thread {} causa {}", t.getName(), e.getMessage());
                    });

                    thread.setPriority(Thread.NORM_PRIORITY);
                    thread.start();

                } else {
                    error("Monitor no esta configurado");
                }
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        systemSocket.connect();

    }

    public static boolean isOfflineProcess() {
        return !systemSocket.isConnected();
    }

    public static boolean isConnected() {
        try {
            return systemSocket.isConnected();
        } catch (Exception e) {
            return false;
        }
    }

    public static void restart() {
        info("Restart monitor");
        if (thread != null) {
            thread.interrupt();
            start();
        }
    }

    public static void stop() {
        info("send kill command");
        writeCmdClose();
        writeCmdKill();
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }

    }

    public static void info(String info) {
        log.info(info);
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG, false);
        if (debug) {
            InputOutput out = IOProvider.getDefault().getIO("spyder", false);
            out.getOut().println(info);
        }
    }

    public static void error(String error) {
        log.info(error);
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG, false);
        if (debug) {
            InputOutput out = IOProvider.getDefault().getIO("spyder", false);
            out.getErr().println(error);
        }
    }

    public static void warn(String warn) {
        log.info(warn);
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG, false);
        if (debug) {
            InputOutput out = IOProvider.getDefault().getIO("spyder", false);
            out.getOut().println(warn);
        }
    }

    public static String formatUniversal(String url) {
        url = url.replaceAll("\\\\", "/");
        if (url.contains("file://")) {
            return url;
        }
        return "file://" + url;
    }

    public static String formatOs(String url) {
        return url.replace("file://", "");
    }

}
