/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.events;

import ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.IdePatch;
import ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.netbeans.NetbeansIdePatch;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options.Spyder;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

import java.io.File;
import java.io.IOException;


public class CompilationBehavior {
    public static final String PATHLOG = "log.rxlog";
    public static final String PATHCONFIG_PROJECT = "nbproject/build-impl.xml";
    public static void configureCompileLogs(Project p) {
        FileObject projectRoot = p.getProjectDirectory();
        FileObject fileObject = projectRoot.getFileObject(PATHCONFIG_PROJECT);
        if(fileObject == null){
            Spyder.error("El tipo proyecto no es soportado");
            return;
        }
        if (!fileObject.isVirtual()) {
            System.out.println("CREANDO");
            try {
                String targetFile = fileObject.getPath();
                FileObject targetLogs = projectRoot.getFileObject("logs");
                if(targetLogs == null){
                    targetLogs = projectRoot.createFolder("logs");   
                }
                String pathlogs = targetLogs.getPath() + File.separatorChar +PATHLOG;
                IdePatch idePatch = new NetbeansIdePatch();
                //idePatch.file(targetFile, pathlogs);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

    }
}
