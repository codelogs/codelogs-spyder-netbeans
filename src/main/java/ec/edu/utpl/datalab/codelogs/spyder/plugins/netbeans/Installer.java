/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans;

import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options.InitDialog;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options.Spyder;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.utils.SyderUtils;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

public class Installer extends ModuleInstall {

    public static InputStream inputStream = System.in;

    @Override
    public void restored() {

        WindowManager.getDefault().invokeWhenUIReady(new Runnable() {
            @Override
            public void run() {
                if (!SyderUtils.isInstalled()) {
                    try {
                        Spyder.info("Instalando monitor");
                        SyderUtils.install();
                    } catch (IOException | URISyntaxException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
                if (!Spyder.isConfig()) {
                    InitDialog.show();
                }
                Spyder.start();
            }
        });
    }

    @Override
    public void close() {
        Spyder.writeCmdClose();
        try {
            Thread.sleep(1000);
            Spyder.stop();
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

}
