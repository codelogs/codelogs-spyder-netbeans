/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import org.netbeans.api.project.Project;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;


public class Projects {

    
    public static void addMeasurement(String name, Project project, NodeProject iProject) {
        measurements.put(name, new AbstractMap.SimpleEntry<Project, NodeProject>(project, iProject));
    }

    
    public static boolean contains(String name) {
        return measurements.containsKey(name);
    }

    
    public static NodeProject getMeasurements(String name) {
        AbstractMap.SimpleEntry<Project, NodeProject> entry = measurements.get(name);
        if (entry != null) {
            return entry.getValue();
        } else {
            return null;
        }
    }
    
    private static Map<String, AbstractMap.SimpleEntry<Project, NodeProject>> measurements =
            new HashMap<String, AbstractMap.SimpleEntry<Project,NodeProject>>();
}
