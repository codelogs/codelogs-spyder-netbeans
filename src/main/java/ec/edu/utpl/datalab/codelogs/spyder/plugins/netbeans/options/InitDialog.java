/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options;

import ec.edu.utpl.datalab.codelogs.spyder.core.util.UtilRx;
import org.openide.util.NbPreferences;

import javax.swing.*;
import java.awt.*;

public class InitDialog {

    public static int show() {
        GridLayout layout = new GridLayout(0, 2);

        JTextField apikey = new JTextField(25);
        JTextField server = new JTextField(25);
        DefaultComboBoxModel boxModel = new DefaultComboBoxModel();
        boxModel.addElement("KWT-Bearer");
        boxModel.addElement("UA-Bearer");
        JComboBox box = new JComboBox(boxModel);

        JPanel myPanel = new JPanel();
        myPanel.setLayout(layout);
        myPanel.add(new JLabel("APIKEY:", 10));
        myPanel.add(apikey);
        myPanel.add(new JLabel("SERVER:", 10));
        myPanel.add(server);
        myPanel.add(new JLabel("protocol:", 10));
        myPanel.add(box);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter sync data", JOptionPane.OK_CANCEL_OPTION);

        if (server.getText().isEmpty() || apikey.getText().isEmpty() || box.getSelectedItem() == null) {
            return -1;
        }

        if (result == JOptionPane.OK_OPTION) {
            NbPreferences.forModule(AjustesPanel.class).put(Global.SYNC_PROTOCOL, box.getSelectedItem().toString());
            NbPreferences.forModule(AjustesPanel.class).put(Global.SYNC_APIKEY, apikey.getText());
            if (UtilRx.isValidUrl(server.getText())) {
                NbPreferences.forModule(AjustesPanel.class).put(Global.SYNC_SERVER, server.getText());
            }
            Spyder.start();
        }
        return 0;
    }

}
