/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author rfcardenas
 */
public class Global {

    /**
     * Propiedades de configuracion de SPYDER
     */
    public static final int DEFAULT_PORT = 8484;
    public static final String APPLICATION_HOME = System.getProperty("user.home") + File.separatorChar + "rxcode";
    public static final String MONITOR_INACTIVE = "monitor.max.inactive";
    public static final String MONITOR_DEBUG = "monitor.max.debug";
    public static final String MONITOR_STEEP = "monitor.cpu.step";
    public static final String MONITOR_SCAN_SOURCE = "monitor.scan.path";
    public static final String MONITOR_PROC_CMD_PORT = "spyder.monitor.port";
    public static final String MONITOR_ENABLE_EVENTSYSTEM_INHERITED = "monitor.eventsystem.inherited";
    public static final String LANG_SUPPORT = "lang.support.dir";
    public static final String SYNC_FORCE = "sync.server.force";
    public static final String SYNC_PROTOCOL = "sync.server.sync.protocol";

    public static final String SYNC_SERVER = "spyder.service.server";
    public static final String SYNC_APIKEY = "spyder.service.token";

    /**
     * ESTRUCTURA DE DIRECTORIO DE PAQUETE SPYDER DIST
     */
    public static final String SPYDER_HOME =  "spyder.home";
    public static final String SPYDER_HOME_CONFIG_FILE = "spyder.home.config.file";
    public static final String SPYDER_HOME_CONFIG_FILE_SUPPORT = "spyder.home.config.support";
    public static final String SPYDER_HOME_DB_PATH = "spyder.home.db";
    /**
     * Mapa de propiedades cargadas
     */
    public static final Map<Object, Object> map = new HashMap<>();

    static {
        map.put(MONITOR_DEBUG, false);
        map.put(MONITOR_INACTIVE, 5000);
        map.put(MONITOR_STEEP, 1000);
        map.put(MONITOR_SCAN_SOURCE, Collections.singletonList("src"));
        map.put(LANG_SUPPORT, "support");
        map.put(MONITOR_PROC_CMD_PORT, 8484);
        map.put(SYNC_SERVER, "http://localhost:8080");
        map.put(SYNC_PROTOCOL, "KWT-Bearer");
        map.put(SYNC_FORCE, false);
        map.put(MONITOR_ENABLE_EVENTSYSTEM_INHERITED, true);

        /**
         * Estructura de directorios
         */
        map.put(SPYDER_HOME_CONFIG_FILE,"config/monitor_daemon.properties");
        map.put(SPYDER_HOME_CONFIG_FILE_SUPPORT, "config/monitor_support.json");
        map.put(SPYDER_HOME_DB_PATH, "db/spyder.db");
    }
}
