/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import org.openide.nodes.PropertySupport;


public class MetricPropertyDouble extends PropertySupport.ReadOnly<Double> implements TooltipProvider {

    public MetricPropertyDouble(String name, Double value, String tooltip) {
        super(name, Double.class, name, name);


        String s = String.format(Locale.US, "%.2f", value);
        this.value = Double.valueOf(s);
        this.tooltip = tooltip;
    }

    @Override
    public Double getValue() throws IllegalAccessException, InvocationTargetException {
        return value;
    }

    @Override
    public String getTooltip() {
        return tooltip;
    }
    
    @Override
    public String toString() {
        if(value != null) {
            return value.toString();
        } else {
            return "";
        }
    }
    private Double value;
    private String tooltip;
}
