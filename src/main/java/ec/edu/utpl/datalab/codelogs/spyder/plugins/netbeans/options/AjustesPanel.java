/*
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options;

import org.openide.util.NbPreferences;

final class AjustesPanel extends javax.swing.JPanel {

    private final AjustesOptionsPanelController controller;

    AjustesPanel(AjustesOptionsPanelController controller) {
        this.controller = controller;
        initComponents();
        preloadData();
        // TODO listen to changes in form fields and call controller.changed()
    }

    public void preloadData(){
        this.chkFSM.setEnabled(true);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblPort = new javax.swing.JLabel();
        jcmMonitorPort = new javax.swing.JComboBox<>();
        lblServer = new javax.swing.JLabel();
        lblToken = new javax.swing.JLabel();
        lblProtocol = new javax.swing.JLabel();
        txtServer = new javax.swing.JTextField();
        txtApikey = new javax.swing.JTextField();
        jcbProtocol = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        lblDebug = new javax.swing.JLabel();
        chkDebug = new javax.swing.JCheckBox();
        lblFSM = new javax.swing.JLabel();
        chkFSM = new javax.swing.JCheckBox();
        lblDownloader = new javax.swing.JLabel();
        chkAutoDownloader = new javax.swing.JCheckBox();

        org.openide.awt.Mnemonics.setLocalizedText(lblPort, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblPort.text_1")); // NOI18N

        javax.swing.DefaultComboBoxModel comboBoxModel = new javax.swing.DefaultComboBoxModel<>();
        for(int i=1;i<=65000;i++){
            comboBoxModel.addElement(i);
        }
        comboBoxModel.setSelectedItem(8484);
        jcmMonitorPort.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "8484", "8585", "9090" }));
        jcmMonitorPort.setModel(comboBoxModel);

        org.openide.awt.Mnemonics.setLocalizedText(lblServer, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblServer.text_2")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(lblToken, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblToken.text_2")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(lblProtocol, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblProtocol.text_2")); // NOI18N

        txtServer.setText(org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.txtServer.text_1")); // NOI18N
        txtServer.setToolTipText(org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.txtServer.toolTipText")); // NOI18N

        txtApikey.setText(org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.txtApikey.text_1")); // NOI18N
        txtApikey.setToolTipText(org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.txtApikey.toolTipText")); // NOI18N

        jcbProtocol.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "KWT-Bearer", "UA-Bearer" }));

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.jButton2.text_1")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jButton3, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.jButton3.text_1")); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(lblDebug, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblDebug.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(chkDebug, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.chkDebug.text")); // NOI18N
        chkDebug.setToolTipText(org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.chkDebug.toolTipText")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(lblFSM, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblFSM.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(chkFSM, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.chkFSM.text")); // NOI18N
        chkFSM.setToolTipText(org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.chkFSM.toolTipText")); // NOI18N
        chkFSM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFSMActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(lblDownloader, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.lblDownloader.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(chkAutoDownloader, org.openide.util.NbBundle.getMessage(AjustesPanel.class, "AjustesPanel.chkAutoDownloader.text")); // NOI18N
        chkAutoDownloader.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblServer)
                            .addComponent(lblToken)
                            .addComponent(lblProtocol)
                            .addComponent(lblPort))
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtServer)
                            .addComponent(txtApikey)
                            .addComponent(jcbProtocol, 0, 189, Short.MAX_VALUE)
                            .addComponent(jcmMonitorPort, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lblDownloader, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addComponent(lblFSM, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblDebug, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(8, 8, 8)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(chkFSM)
                                    .addComponent(chkDebug)
                                    .addComponent(chkAutoDownloader))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblServer)
                    .addComponent(txtServer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblToken)
                    .addComponent(txtApikey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProtocol)
                    .addComponent(jcbProtocol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPort)
                    .addComponent(jcmMonitorPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFSM)
                    .addComponent(chkFSM))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDebug)
                    .addComponent(chkDebug))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDownloader)
                    .addComponent(chkAutoDownloader))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void chkFSMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFSMActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_chkFSMActionPerformed

    void load() {
        // TODO read settings and initialize GUI
        // Example:        
        // someCheckBox.setSelected(Preferences.userNodeForPackage(AjustesPanel.class).getBoolean("someFlag", false));
        // or for org.openide.util with API spec. version >= 7.4:
        // someCheckBox.setSelected(NbPreferences.forModule(AjustesPanel.class).getBoolean("someFlag", false));
        // or:
        // someTextField.setText(SomeSystemOption.getDefault().getSomeStringProperty());
        String keyap = Global.SYNC_APIKEY;
        String keyServer = Global.SYNC_SERVER;
        String keyProtocol = Global.SYNC_PROTOCOL;
        String cmdPort = Global.MONITOR_PROC_CMD_PORT;
        String fsmEnable = Global.MONITOR_ENABLE_EVENTSYSTEM_INHERITED;
        String apikeyValue = NbPreferences.forModule(AjustesPanel.class).get(keyap, "");
        String protocolValue = NbPreferences.forModule(AjustesPanel.class).get(keyProtocol, "");
        String serverValue = NbPreferences.forModule(AjustesPanel.class).get(keyServer, "");
        String cmdportValue = NbPreferences.forModule(AjustesPanel.class).get(cmdPort, "");
        boolean fsminherited = NbPreferences.forModule(AjustesPanel.class).getBoolean(fsmEnable, true);
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG,false);
        txtApikey.setText(apikeyValue);
        txtServer.setText(serverValue);
        jcbProtocol.setSelectedItem(protocolValue);
        jcmMonitorPort.setSelectedItem(cmdportValue);
        chkDebug.setSelected(debug);
        chkFSM.setSelected(fsminherited);
    }

    void store() {
        // TODO store modified settings
        // Example:
        // Preferences.userNodeForPackage(AjustesPanel.class).putBoolean("someFlag", someCheckBox.isSelected());
        // or for org.openide.util with API spec. version >= 7.4:
        // NbPreferences.forModule(AjustesPanel.class).putBoolean("someFlag", someCheckBox.isSelected());
        // or:
        // SomeSystemOption.getDefault().setSomeStringProperty(someTextField.getText());
         String apikey = txtApikey.getText();
        String protocol = jcbProtocol.getSelectedItem().toString();
        String server = txtServer.getText();
        Integer portcmd = Integer.parseInt(jcmMonitorPort.getSelectedItem().toString());
        boolean debug = chkDebug.isSelected();
        boolean fmsmonitor = chkFSM.isSelected();
        NbPreferences.forModule(AjustesPanel.class).put(Global.SYNC_APIKEY, apikey);
        NbPreferences.forModule(AjustesPanel.class).put(Global.SYNC_PROTOCOL, protocol);
        NbPreferences.forModule(AjustesPanel.class).put(Global.SYNC_SERVER, server);
        NbPreferences.forModule(AjustesPanel.class).put(Global.MONITOR_PROC_CMD_PORT, String.valueOf(portcmd));
        NbPreferences.forModule(AjustesPanel.class).putBoolean(Global.MONITOR_DEBUG, debug);
        NbPreferences.forModule(AjustesPanel.class).putBoolean(Global.MONITOR_ENABLE_EVENTSYSTEM_INHERITED, fmsmonitor);
        
    }

    boolean valid() {
        // TODO check whether form is consistent and complete
        return true;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkAutoDownloader;
    private javax.swing.JCheckBox chkDebug;
    private javax.swing.JCheckBox chkFSM;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jcbProtocol;
    private javax.swing.JComboBox<String> jcmMonitorPort;
    private javax.swing.JLabel lblDebug;
    private javax.swing.JLabel lblDownloader;
    private javax.swing.JLabel lblFSM;
    private javax.swing.JLabel lblPort;
    private javax.swing.JLabel lblProtocol;
    private javax.swing.JLabel lblServer;
    private javax.swing.JLabel lblToken;
    private javax.swing.JTextField txtApikey;
    private javax.swing.JTextField txtServer;
    // End of variables declaration//GEN-END:variables
}
