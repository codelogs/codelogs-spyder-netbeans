/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.PublicResult;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeClase;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes.factories.ClassChildFactory;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;


public class ClassNode extends AbstractNode {

    private PublicResult metricResult;

    public ClassNode(NodeClase c, PublicResult metricResult) {
        super(Children.create(new ClassChildFactory(c, metricResult), true));
        this.c = c;
        this.metricResult = metricResult;
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        if (c != null) {
            for (MetricVerticle metric : metricResult.loadAllMetrics(c.getClass())) {
                Measure result = metricResult.getMeasureFor(c, metric);
                System.out.println("--> " + result);
                if (result != null) {
                    System.out.println("Measure CLASS>>>" + result.getResult());
                    String name = metric.getSpecification().getName();
                    String desc = metric.getSpecification().getHelp();
                    set.put(new MetricPropertyDouble(name, result.getResult(), desc));
                }
            }
        }
        System.out.println(set);
        sheet.put(set);
        return sheet;
    }

    @Override
    public Action getPreferredAction() {
        return new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("ACTION....");
                JOptionPane.showMessageDialog(null, "Actions");
                
            }
        };
    }

    @Override
    public String getName() {
        if (c == null) {
            return "Error!";
        } else {
            return c.getName();
        }
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage("ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/icons/class.png");
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getIcon(type);
    }
    private NodeClase c;
    private final static Logger logger = Logger.getLogger(ClassNode.class.getName());
}
