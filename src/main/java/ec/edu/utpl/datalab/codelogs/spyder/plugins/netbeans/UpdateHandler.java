/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans;

/**
 *
 * @Author ronald
 */
/**
public final class UpdateHandler {

    public static boolean timeToCheck() {
        // every startup
        return true;
    }

    public static class UpdateHandlerException extends Exception {
        public UpdateHandlerException(String msg) {
            super(msg);
        }
        public UpdateHandlerException(String msg, Throwable th) {
            super(msg, th);
        }
    }

    public static void checkAndHandleUpdates() {

        Spyder.info("Checking for updates to Spyder plugin...");

        // refresh silent update center first
        refreshSilentUpdateProvider();

        Collection<UpdateElement> updates = findUpdates();
        Collection<UpdateElement> available = Collections.emptySet();
        if (installNewModules()) {
            available = findNewModules();
        }
        if (updates.isEmpty() && available.isEmpty()) {
            // none for install
            Spyder.info("Spyder plugin is up to date.");
            return;
        }

        Spyder.info("Found new Spyder plugin version, updating...");

        // create a container for install
        OperationContainer<InstallSupport> containerForInstall = feedContainer(available, false);
        if (containerForInstall != null) {
            try {
                handleInstall(containerForInstall);
                Spyder.info("Spyder plugin installation finished.");
            } catch (UpdateHandlerException ex) {
                Spyder.error(ex.toString());

                // cancel progress bar
                InstallSupport support = containerForInstall.getSupport();
                try {
                    support.doCancel();
                } catch (OperationException ex1) {
                    Spyder.error(ex1.toString());
                }

                return;
            }
        }

        // create a container for update
        OperationContainer<InstallSupport> containerForUpdate = feedContainer(updates, true);
        if (containerForUpdate != null) {
            try {
                handleInstall(containerForUpdate);
                Spyder.info("Spyder plugin update finished.");
            } catch (UpdateHandlerException ex) {
                Spyder.error(ex.toString());

                // cancel progress bar
                InstallSupport support = containerForUpdate.getSupport();
                try {
                    support.doCancel();
                } catch (OperationException ex1) {
                    Spyder.error(ex1.toString());
                }

                return;
            }
        }

    }

    public static boolean isLicenseApproved(String license) {
        // place your code there
        return true;
    }

    // package private methods
    static Collection<UpdateElement> findUpdates() {
        // check updates
        Collection<UpdateElement> elements4update = new HashSet<UpdateElement>();
        List<UpdateUnit> updateUnits = UpdateManager.getDefault().getUpdateUnits();
        for (UpdateUnit unit : updateUnits) {
            if (unit.getInstalled() != null) { // means the plugin already installed
                if (unit.getCodeName().equals(Spyder.CODENAME)) { // this is our current plugin
                    if (!unit.getAvailableUpdates().isEmpty()) { // has updates
                        elements4update.add(unit.getAvailableUpdates().get(0)); // add plugin with highest version
                    }
                }
            }
        }
        return elements4update;
    }

    static void handleInstall(OperationContainer<InstallSupport> container) throws UpdateHandlerException {
        // check licenses
        if (!allLicensesApproved(container)) {
            // have a problem => cannot continue
            throw new UpdateHandlerException("Cannot continue because license approval is missing.");
        }
        
        InstallSupport support = container.getSupport();

        // download
        Validator v = null;
        try {
            v = doDownload(support);
        } catch (OperationException ex) {
            throw new UpdateHandlerException("A problem caught while downloading, cause: ", ex);
        } catch (NullPointerException ex) {
            throw new UpdateHandlerException("A problem caught while downloading, cause: ", ex);
        }
        if (v == null) {
            // have a problem => cannot continue
            throw new UpdateHandlerException("Missing Update Validator => cannot continue.");
        }

        // verify
        Installer i = null;
        try {
            i = doVerify(support, v);
        } catch (OperationException ex) {
            // caught a exception
            throw new UpdateHandlerException("A problem caught while verification of updates, cause: ", ex);
        }
        if (i == null) {
            // have a problem => cannot continue
            throw new UpdateHandlerException("Missing Update Installer => cannot continue.");
        }

        // install
        Restarter r = null;
        try {
            r = doInstall(support, i);
        } catch (OperationException ex) {
            // caught a exception
            throw new UpdateHandlerException("A problem caught while installation of updates, cause: ", ex);
        }

        // restart later
        support.doRestartLater(r);
    }

    static Collection<UpdateElement> findNewModules() {
        // check updates
        Collection<UpdateElement> elements4install = new HashSet<UpdateElement>();
        List<UpdateUnit> updateUnits = UpdateManager.getDefault().getUpdateUnits();
        for (UpdateUnit unit : updateUnits) {
            if (unit.getInstalled() == null) { // means the plugin is not installed yet
                if (unit.getCodeName().equals(Spyder.CODENAME)) { // this is our current plugin
                    if (!unit.getAvailableUpdates().isEmpty()) { // is available
                        elements4install.add(unit.getAvailableUpdates().get(0)); // add plugin with highest version
                    }
                }
            }
        }
        return elements4install;
    }

    static void refreshSilentUpdateProvider() {
        UpdateUnitProvider silentUpdateProvider = getSilentUpdateProvider();
        if (silentUpdateProvider == null) {
            // have a problem => cannot continue
            Spyder.info("Missing Silent Update Provider => cannot continue.");
            return ;
        }
        try {
            final String displayName = "Checking for updates to Spyder plugin...";
            silentUpdateProvider.refresh(
                ProgressHandleFactory.createHandle(
                    displayName,
                    new Cancellable () {
                        @Override
                        public boolean cancel () {
                            return true;
                        }
                    }
                ),
                true
            );
        } catch (IOException ex) {
            // caught a exception
            Spyder.error("A problem caught while refreshing Update Centers, cause: " + ex.toString());
        }
    }

    static UpdateUnitProvider getSilentUpdateProvider() {
        List<UpdateUnitProvider> providers = UpdateUnitProviderFactory.getDefault().getUpdateUnitProviders(true);
        String oldCodename = "org_wakatime_netbeans_plugin_update_center";
        for (UpdateUnitProvider p : providers) {
            if (p.getName().equals(oldCodename) || p.getName().equals(Spyder.CODENAME)) { // this is our current plugin
                try {
                    final String displayName = "Checking for updates to Spyder plugin...";
                    p.refresh(
                        ProgressHandleFactory.createHandle(
                            displayName,
                            new Cancellable () {
                                @Override
                                public boolean cancel () {
                                    return true;
                                }
                            }
                        ),
                        true
                    );
                } catch (IOException ex) {
                    // caught a exception
                    Spyder.error("A problem caught while refreshing Update Centers, cause: " + ex.toString());
                }
                return p;
            }
        }
        return null;
    }

    static OperationContainer<InstallSupport> feedContainer(Collection<UpdateElement> updates, boolean update) {
        if (updates == null || updates.isEmpty()) {
            return null;
        }
        // create a container for update
        OperationContainer<InstallSupport> container;
        if (update) {
            container = OperationContainer.createForUpdate();
        } else {
            container = OperationContainer.createForInstall();
        }

        // loop all updates and add to container for update
        for (UpdateElement ue : updates) {
            if (container.canBeAdded(ue.getUpdateUnit(), ue) && ue.getCodeName().equals(Spyder.CODENAME)) {
                Spyder.info("Update to Spyder plugin found: " + ue);
                OperationInfo<InstallSupport> operationInfo = container.add(ue);
                if (operationInfo == null) {
                    continue;
                }
                container.add(operationInfo.getRequiredElements());
                if (!operationInfo.getBrokenDependencies().isEmpty()) {
                    // have a problem => cannot continue
                    Spyder.info("There are broken dependencies => cannot continue, broken deps: " + operationInfo.getBrokenDependencies());
                    return null;
                }
            }
        }
        return container;
    }

    static boolean allLicensesApproved(OperationContainer<InstallSupport> container) {
        if (!container.listInvalid().isEmpty()) {
            return false;
        }
        for (OperationInfo<InstallSupport> info : container.listAll()) {
            String license = info.getUpdateElement().getLicence();
            if (!isLicenseApproved(license)) {
                return false;
            }
        }
        return true;
    }

    static Validator doDownload(InstallSupport support) throws OperationException {
        final String displayName = "Downloading new version of Spyder plugin...";
        ProgressHandle downloadHandle = ProgressHandleFactory.createHandle(
            displayName,
            new Cancellable () {
                @Override
                public boolean cancel () {
                    return true;
                }
            }
        );
        return support.doDownload(downloadHandle, true);
    }

    static Installer doVerify(InstallSupport support, Validator validator) throws OperationException {
        final String displayName = "Validating Spyder plugin...";
        ProgressHandle validateHandle = ProgressHandleFactory.createHandle(
            displayName,
            new Cancellable () {
                @Override
                public boolean cancel () {
                    return true;
                }
            }
        );
        Installer installer = support.doValidate(validator, validateHandle);
        return installer;
    }

    static Restarter doInstall(InstallSupport support, Installer installer) throws OperationException {
        final String displayName = "Installing Spyder plugin...";
        ProgressHandle installHandle = ProgressHandleFactory.createHandle(
            displayName,
            new Cancellable () {
                @Override
                public boolean cancel () {
                    return true;
                }
            }
        );
        return support.doInstall(installer, installHandle);
    }

    private static boolean installNewModules() {
    //    String s = NbBundle.getBundle("org.wakatime.netbeans.plugin.Bundle").getString("UpdateHandler.NewModules");
        return Boolean.parseBoolean("");
    }
}
**/