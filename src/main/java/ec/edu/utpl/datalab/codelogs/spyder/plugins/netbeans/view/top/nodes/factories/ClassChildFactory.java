/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes.factories;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.PublicResult;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeClase;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeMethod;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes.MethodNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

import java.util.List;
import java.util.stream.StreamSupport;


public class ClassChildFactory extends ChildFactory<NodeMethod> {

    private NodeClase clase;
    private PublicResult metricResult;

    public ClassChildFactory(NodeClase clase, PublicResult metricResult) {
        this.clase = clase;
        this.metricResult = metricResult;
    }

    @Override
    protected Node createNodeForKey(NodeMethod key) {
        System.out.println("Creando nuevo nodo paquete");
        return new MethodNode(key, metricResult);
    }

    @Override
    protected boolean createKeys(List<NodeMethod> list) {
        if (clase != null) {
            TreeTypeSpliterator<NodeMethod> spliterator = TreeTypeSpliterator.create(clase, NodeMethod.class);
            StreamSupport.stream(spliterator, false)
                    .forEach(list::add);
        }
        return true;
    }

}
