/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes;

import java.awt.Color;
import java.awt.Component;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreeNode;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.options.limitations.MetricConfiguration;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.options.limitations.MetricConfigurations;


public class MyTableCellRenderer extends JLabel implements TableCellRenderer {

    public MyTableCellRenderer() {
        hsbColor = Color.RGBtoHSB(51, 153, 255, null);
        blueColor = Color.getHSBColor(hsbColor[0], hsbColor[1], hsbColor[2]);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        lbl = new JLabel();

        if (value != null) {


            String scope = "";
            TreeNode o = (TreeNode) table.getValueAt(row, 0);
            Node node = Visualizer.findNode(o);
            if (node instanceof PackageNode) {
                scope = "package";
            } else if (node instanceof ProjectNode) {
                scope = "project";
            }


            TableColumnModel tcm = table.getColumnModel();
            TableColumn tc = tcm.getColumn(column);
            String metricName = tc.getHeaderValue().toString() + " " + scope;


            Map<String, MetricConfiguration> mm = MetricConfigurations.getMc().getMetricConfigurationsMap();
            MetricConfiguration mc = mm.get(metricName);

            Double mv = null;
            try {
                mv = Double.valueOf(value.toString());
            } catch (NumberFormatException ex) {
                lbl.setText(value.toString());
                lbl.setToolTipText(value.toString());
                return lbl;
            }


            if (mc != null) {
                if (mv < mc.getMinimum() || mc.getMaximum() < mv) {
                    lbl.setForeground(Color.red);
                }
            }

            String tooltip = value.toString();
            if (value instanceof TooltipProvider) {
                TooltipProvider tp = (TooltipProvider) value;
                tooltip = tp.getTooltip();
            }

            lbl.setText(value.toString());
            lbl.setToolTipText(tooltip);

            if (isSelected) {
                lbl.setOpaque(true);
                lbl.setBackground(blueColor);
            }
        } else {
            lbl.setText("");
        }
        
        return lbl;
    }
    private JLabel lbl;
    private static float[] hsbColor;
    private static Color blueColor;
}
