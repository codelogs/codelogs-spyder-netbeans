/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top;

import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.CodeAnalysisPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatCodeMeasurePack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.TYPE_INSTRUCTION_PACK;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.PublicResult;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor.ProcessorResult;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options.Spyder;
import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.view.top.nodes.ProjectNode;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.Project;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@ActionID(category = "Tools",
        id = "metrics.view.MetricsAction")
@ActionRegistration(iconBase = "ec/edu/utpl/datalab/codelogs/spyder/plugins/netbeans/view/top/magnifier.gif",
        displayName = "#CTL_MetricsAction")
@ActionReferences({
    @ActionReference(path = "Menu/Tools", position = 190),
    @ActionReference(path = "Projects/Actions", position = 1400, separatorBefore = 1350, separatorAfter = 1450)
})
@Messages("CTL_MetricsAction=Source Code Metrics")
public final class MetricsAction implements ActionListener {

    private static final Logger logger = Logger.getLogger(MetricsAction.class.getName());

    private NodeProject p;
    private PublicResult results;
    private final Project context;
    /**
     * Allows to run only one task at a time and tasks are interruptible.
     */
    private final static RequestProcessor RP = new RequestProcessor("interruptible tasks", 1, true);

    /**
     * Used to measure the metrics of the project.
     */
    public MetricsAction(Project context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {

        Runnable r = new Runnable() {

            @Override
            public void run() {

                // Initlization and measurement
                NodeProject project = null;
                try {
                    FileObject fileObject = VFS.getManager().resolveFile(context.getProjectDirectory().getPath());

                    Optional<NodeProject> result = MetricsNames.plataforma.generadores().create(fileObject);
                    PublicResult metricResult;

                    MetricsNames.plataforma.processor().observable()
                            .subscribe((ProcessorResult t) -> {
                                results = t.getResultHolder();
                                sendSummary(results, p);
                            });
                    if (result.isPresent()) {
                        p = result.get();
                        MetricsNames.plataforma.processor().process(p);
                    }

                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Error. Couldn't perform the measurements, due to the exception.", e);
                    NotifyDescriptor d
                            = new NotifyDescriptor.Message(
                                    "Error. Couldn't perform the measurements, due to "
                                    + "the exception: \r\n" + e.getMessage(), NotifyDescriptor.ERROR_MESSAGE);
                    DialogDisplayer.getDefault().notify(d);
                    return;
                }

                // make sure that the returned project is valid
                if (p == null) {
                    NotifyDescriptor d
                            = new NotifyDescriptor.Message(
                                    "The returned project is null.", NotifyDescriptor.ERROR_MESSAGE);
                    DialogDisplayer.getDefault().notify(d);
                    return;
                }
            }
        };

        final RequestProcessor.Task measurementTask = RP.create(r);

        final ProgressHandle ph = ProgressHandleFactory.createHandle("Measuring...", measurementTask);
        measurementTask.addTaskListener(new TaskListener() {
            @Override
            public void taskFinished(Task task) {
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        if (p != null) {
                            // set the new context in the metrics top component
                            MetricsWindowTopComponent mwtc = MetricsWindowTopComponent.getSelf();
                            mwtc.getExplorerManager().setRootContext(new ProjectNode(p, results));
                            mwtc.setDisplayName("Source Code Metrics of " + p.getName());
                            // remember the measurements of the given project
                            Projects.addMeasurement(context.getProjectDirectory().getName(), context, p);
                            // show the metrics window top component
                            if (!mwtc.isOpened()) {
                                mwtc.open();
                            }
                            mwtc.setMessage(true, "");
                        }
                    }
                });

                // progress indicator to finish when the measurement has been finished
                ph.finish();
            }
        });

        ph.start();
        measurementTask.schedule(0); // start the measurement
    }

    private void sendSummary(PublicResult metricResult, NodeProject p) {
        CodeAnalysisPack analysis = new CodeAnalysisPack();
        HashSet<HeartBeatCodeMeasurePack> mesureDtos = new HashSet<>();
        analysis.setPathContext("file://" + p.getFilePath());

        List<MetricVerticle> metrics = MetricsNames.plataforma.metricRepository().getAllMetricWithTarget(NodeKink.PROJECT);
        for (MetricVerticle metric : metrics) {
            String uuid = metric.getSpecification().getUuid();
            Measure result = results.getMeasureFor(p, metric);
            HeartBeatCodeMeasurePack dto = new HeartBeatCodeMeasurePack();
            dto.setInstruction(TYPE_INSTRUCTION_PACK.HEART_BEAT_CODE_ACTIVITY);
            dto.setValue(result.getResult());
            dto.setMetric(new GrantedContextPack(uuid));
            mesureDtos.add(dto);
        }
        analysis.setHeartBeatCodeMeasures(mesureDtos);
        Spyder.writeCmdMetricsCollect(analysis);

    }

}
