/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.utils;

import ec.edu.utpl.datalab.codelogs.spyder.plugins.netbeans.options.Spyder;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

/**
 *
 * @author rfcardenas
 */
public class SyderUtils {
    private static final Logger log = LoggerFactory.getLogger(Spyder.class);

    public static final String FOLDER_NAME_SPYDER = "spyder_nbm";
    public static final String HOME_SPYDER = System.getProperty("user.home");
    public static final String SPYDER_SERVICE_JAR = "codelogs-spyder-osservice";

    public static boolean isInstalled() {
        File f = new File(HOME_SPYDER, FOLDER_NAME_SPYDER);
        return f.exists();
    }

    public static void install() throws IOException, URISyntaxException {
        log.info("Instalando spyder en {}",FOLDER_NAME_SPYDER);


        
         
        ClassLoader cl = Lookup.getDefault().lookup(ClassLoader.class);
        String pathToImageSortBy = "dist.zip";

        String a = cl.getResource(pathToImageSortBy).getProtocol();
        System.out.println(cl.getResource(pathToImageSortBy).getRef());
        System.out.println(cl.getResource(pathToImageSortBy).getFile());
        System.out.println(cl.getResource(pathToImageSortBy).getPath());
        System.out.println(cl.getResource(pathToImageSortBy).getQuery());
        System.out.println(cl.getResource(pathToImageSortBy).getUserInfo());

        String path = cl.getResource(pathToImageSortBy).getPath();
        path = path.replaceAll("file:", "jar:");

        FileObject zipFile = VFS.getManager().resolveFile(path);
        
        
        System.out.println(path);
        System.out.println(zipFile.exists());
        System.out.println(zipFile.isReadable());
        
        
        InputStream ax = zipFile.getContent().getInputStream();
        Unzip unzip  = new Unzip();
        unzip.unZipIt(ax, HOME_SPYDER + File.separatorChar+FOLDER_NAME_SPYDER);
    }

}
